export const state = () =>({
  user:{
    data:[]
  }
})

export const mutations ={
  setUser(state, data){
    state.user = data
  }
}

export const actions={
  selectUser({commit}, params){
    this.$axios.get('https://reqres.in/api/users?',{params}).then((data)=>{
      commit('setUser', data.data)
    })
  }
}
